import {NgModule} from '@angular/core';
import {ContactComponent} from './components/contact/contact.component';
import {CardComponent} from './components/card/card.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SectionComponent} from './components/section/section.component';

@NgModule({
    imports: [
        ReactiveFormsModule,
        CommonModule
    ],
  declarations: [
    ContactComponent,
    CardComponent,
    SectionComponent
  ],
  exports: [
    ContactComponent,
    CardComponent,
    SectionComponent
  ]
})
export class SharedModule { }
