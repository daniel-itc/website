import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ditc-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {
  constructor() { }

  readonly form: FormGroup = new FormGroup({
    fullName: new FormControl('', Validators.required),
    // TODO: Vat number validation
    vatNumber: new FormControl(),
    contact: new FormGroup({
      email: new FormControl('', Validators.email),
      phoneNumber: new FormControl()
    }),
    budget: new FormControl(),
    description: new FormControl(),
    serviceType: new FormControl('Other...', Validators.required),
    sendNewsConsent: new FormControl(false)
  });

  onFormReset(): void {
    this.form.reset();
  }

  onFormSubmit(): void {
    if (this.form.invalid) {
      // TODO: display error message

      return;
    }

    // TODO: save the request
  }
}
