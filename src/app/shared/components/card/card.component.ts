import {Component, Input} from '@angular/core';

@Component({
  selector: 'ditc-card',
  template: `
    <div class="ditc-card__inner">
        <div class="ditc-card__inner-content">
          <span *ngIf="isNew" class="ditc-card__new" i18n>NEW!</span>
          <span>{{serviceName}}</span>
        </div>
        <div class="ditc-card__inner-content ditc-card__inner-content-back">
          <button class="ditc-button-primary" i18n>Get more</button>
        </div>
    </div>
  `,
  styleUrls: ['./card.component.scss']
})
export class CardComponent {

  @Input()
  serviceName: string;

  @Input()
  isNew = false;
}
