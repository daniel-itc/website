import {EventEmitter, OnDestroy} from '@angular/core';

export abstract class BaseComponent implements OnDestroy {
  protected readonly onDestroy$ = new EventEmitter<void>();

  ngOnDestroy(): void {
    this.onDestroy$.emit();
  }
}
