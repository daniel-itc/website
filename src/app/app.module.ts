import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAnalyticsModule, CONFIG, ScreenTrackingService, UserTrackingService} from '@angular/fire/analytics';

import '@angular/common/locales/global/it';
import {SharedModule} from './shared/shared.module';
import {AppRoutingModule} from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
      BrowserModule.withServerTransition({appId: 'serverApp'}),
      ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
      AngularFireModule.initializeApp(environment.firebase),
      AngularFireAnalyticsModule,
      AppRoutingModule,
      SharedModule,
    ],
  providers: [
    { provide: LOCALE_ID, useValue: 'en' },
    ScreenTrackingService,
    UserTrackingService,
    {
      provide: CONFIG, useValue: {
        anonymize_ip: true
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
