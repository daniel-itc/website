import {Component} from '@angular/core';
import * as homePageConstants from '../index';

@Component({
  selector: 'ditc-top-bar',
  template: `
    <div class="ditc-top-bar-background">
      <div class="ditc-container">
        <div class="ditc-top-bar">
          <picture>
            <source media="(min-width:465px)" srcset="assets/images/logo.png" type="image/png">
            <img height="56" src="assets/images/logo.png" alt="Daniel ITC Logo">
          </picture>

          <div class="ditc-top-bar__menu-items ditc-top-bar__menu-items-right">
            <a routerLink="/home" [fragment]="aboutMeFragment" i18n>Who I'm?</a>
            <a routerLink="/home" [fragment]="customersFragment" i18n>Customers</a>
            <a routerLink="/home" [fragment]="projectFragment" i18n>Projects</a>
            <a routerLink="/home" [fragment]="servicesFragment" i18n>Services</a>

            <button routerLink="/home" [fragment]="contactFragment" class="ditc-button-primary" type="button" i18n>Contact me!</button>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent {

  readonly aboutMeFragment = homePageConstants.aboutMeFragment;
  readonly customersFragment = homePageConstants.customersFragment;
  readonly projectFragment = homePageConstants.projectsFragment;
  readonly servicesFragment = homePageConstants.servicesFragment;
  readonly contactFragment = homePageConstants.contactFragment;
}
