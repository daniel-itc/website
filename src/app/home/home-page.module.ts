import {NgModule} from '@angular/core';
import {ContactSectionComponent} from './sections/contact/contact-section.component';
import {PartnersSectionComponent} from './sections/parthners/partners-section.component';
import {PortfolioSectionComponent} from './sections/portfoglio/portfolio-section.component';
import {ServicesSectionComponent} from './sections/services/services-section.component';
import {TargetSectionComponent} from './sections/target/target-section.component';
import {HomePageRoutingModule} from './home-page-routing.module';
import {HomePageComponent} from './home-page.component';
import {TopBarComponent} from './top-bar/top-bar.component';
import {ServiceComponent} from './sections/services/components/service.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {AboutMeSectionComponent} from './sections/about-me/about-me-section.component';
import {CodeEditorComponent} from './sections/about-me/code-editor/code-editor.component';

@NgModule({
  imports: [CommonModule, HomePageRoutingModule, SharedModule],
  declarations: [
    HomePageComponent,
    ContactSectionComponent,
    PartnersSectionComponent,
    PortfolioSectionComponent,
    ServicesSectionComponent,
    ServiceComponent,
    TargetSectionComponent,
    TopBarComponent,
    AboutMeSectionComponent,
    CodeEditorComponent
  ],
  // exports: [HomePageComponent]
})
export class HomePageModule { }
