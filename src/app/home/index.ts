
export const aboutMeFragment = 'who-i-m';
export const customersFragment = 'customers';
export const projectsFragment = 'projects';
export const servicesFragment = 'services';
export const contactFragment = 'contact';
