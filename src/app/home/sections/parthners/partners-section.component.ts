import {Component} from '@angular/core';

@Component({
  selector: 'ditc-partners-section',
  template: `
    <div class="ditc-partners-section">
      <div class="ditc-container">
        <h3 class="ditc-title" i18n>Companies I have worked with</h3>

        <div class="ditc-partners-section__partners">
          <div class="ditc-partners-section__partner">
            <img height="66" src="assets/images/partner-kaatstudio.png">
          </div>
          <div class="ditc-partners-section__partner">
            <img height="66" src="assets/images/partner-protesisimmobiliare.png">
          </div>
          <div class="ditc-partners-section__partner">
            <img height="66" src="assets/images/partner-corfi.png">
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./partners-section.component.scss']
})
export class PartnersSectionComponent {

}
