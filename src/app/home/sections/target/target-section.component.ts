import {Component} from '@angular/core';

@Component({
  selector: 'ditc-target-section',
  template: `
    <div class="ditc-container ditc-target-section">
      <h3 class="ditc-title" i18n>Who I turn to</h3>

      <p class="ditc-body" i18n>Are you a business, a restaurant, a small business or startup? Contact me to develop the best management system or website.</p>

      <div class="ditc-target-section__targets">
        <div class="ditc-target-section__target ">
          <img src="assets/images/sample-restaurant.png">
        </div>
        <div class="ditc-target-section__target">
          <img src="assets/images/sample-small-business.png">
        </div>
        <div class="ditc-target-section__target">
          <img src="assets/images/sample-startup.png">
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./target-section.component.scss']
})
export class TargetSectionComponent {

}
