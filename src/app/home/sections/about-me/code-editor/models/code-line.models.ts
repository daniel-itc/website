
export interface CodeLine {
  tag: string;
  closeTag: boolean;
  idName?: string | undefined;
  className?: string | undefined;
  referenceName?: string | undefined;
  indentationSpaces?: number | undefined;
  content?: string | undefined;
}

export type CodeLines = CodeLine[];
