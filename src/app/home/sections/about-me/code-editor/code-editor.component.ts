import {Component, Input, OnInit} from '@angular/core';
import {CodeLines} from './models/code-line.models';

@Component({
  selector: 'ditc-code-editor',
  template: `
    <div class="ditc-code-editor">
      <div class="ditc-code-editor__lines-counter">
        <span *ngFor="let lineNumber of linesNumbers">{{lineNumber}}</span>
      </div>
      <div class="ditc-code-editor__code-lines">
        <!--      TODO: set the indentation-->
        <!-- Print code and content -->
        <code *ngFor="let codeLine of codeLines; index as i">
          &lt;<span class="ditc-code-editor__tag">{{codeLine.tag}}</span>
          <span *ngIf="codeLine.referenceName" class="ditc-code-editor__reference">
          #<span class="ditc-code-editor__reference-name">{{codeLine.referenceName}}</span>
        </span>
          <span *ngIf="codeLine.idName" class="ditc-code-editor__id">
          id=&quot;<span class="ditc-code-editor__id-name">{{codeLine.idName}}</span>&quot;</span>
          <span *ngIf="codeLine.className" class="ditc-code-editor__class">
          class=&quot;<span class="ditc-code-editor__class-name">{{codeLine.className}}</span>&quot;</span>
          <span *ngIf="codeLine.content" class="ditc-code-editor__comment">&nbsp;{{codeLine.content}}&nbsp;</span>&gt;
        </code>

        <!-- Print close tags -->
        <ng-container *ngFor="let codeLine of codeLines">
          <code *ngIf="codeLine.closeTag">&lt;/<span class="ditc-code-editor__tag">{{codeLine.tag}}</span>&gt;</code>
        </ng-container>

        <!--        <ng-container *ngFor="let x of [].constructor(codeLine.identSpaces)">&nbsp;</ng-container>-->
      </div>
    </div>
  `,
  styleUrls: ['./code-editor.component.scss']
})
export class CodeEditorComponent implements OnInit {

  @Input()
  lineCountStart = 1;

  @Input()
  codeLines: CodeLines = [];

  linesNumbers: number[] = [];

  ngOnInit(): void {
    const lastLine = this.codeLines.length + this.lineCountStart;
    for (let i = this.lineCountStart; i <= lastLine; i++) {
      this.linesNumbers.push(i);
    }
  }
}
