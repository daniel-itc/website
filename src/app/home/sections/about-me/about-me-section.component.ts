import {Component} from '@angular/core';
import {CodeLines} from './code-editor/models/code-line.models';

@Component({
  selector: 'ditc-about-me-section',
  template: `
    <div class="ditc-container">
      <div class="ditc-about-me">
        <div class="ditc-about-me__row ditc-about-me__presentation">
          <div style="margin-top: 7.5rem; flex-grow: 1" class="ditc-home__card">
            <ditc-code-editor [lineCountStart]="22" [codeLines]="codeLines"></ditc-code-editor>
          </div>
          <div style="display: flex; flex-direction: column; padding-left: 2.5rem; max-width: 27.25rem">
            <span class="ditc-title" i18n>Hi! My name is <strong>Daniel Zarioiu,</strong></span>
            <span class="ditc-home__body"
                  i18n>I'm a <strong>developer</strong> and I'm here to help you inside the IT world.</span>
          </div>
        </div>


        <div class="ditc-about-me__row">
          <div style="display: flex; flex-direction: column; max-width: 30.75rem">
            <span style="margin-bottom: 1rem; max-width: 30.75rem; flex-wrap: nowrap" class="ditc-title" i18n><strong>Codding with PASSION from 7 years</strong></span>
<!--            TODO: add cv-->
            <p style="margin-bottom: 2rem" class="ditc-home__body" i18n>I always try to keep my self updated reading articles and flowing courses. If you're interest to know more about my career <a href="#" rel="author" type="application/pdf" target="_blank">find out my CV</a>.</p>
            <div>
              <a href="https://www.linkedin.com/in/daniel-m-zarioiu-b4227412a" target="_blank" style="margin-right: 2rem">
                <img height="70" src="assets/icons/linkedin-icon.png" alt="LinkedIn Icon" loading="lazy">
              </a>
              <a href="https://gitlab.com/daniel-itc" target="_blank" style="margin-right: 2rem; display: inline">
                <img height="71" src="assets/icons/gitlab-icon.png" alt="GitLab Icon" loading="lazy">
              </a>
              <a href="https://github.com/daniel-ITC" target="_blank" style="margin-right: 2rem; display: inline">
                <img height="70" src="assets/icons/github-icon.png" alt="GitHub Icon" loading="lazy">
              </a>
            </div>
          </div>
          <div class="ditc-about-me__programming-image">
            <img src="assets/images/night-developer.png" alt="Programming" loading="lazy">
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./about-me-section.component.scss']
})
export class AboutMeSectionComponent {
  readonly codeLines: CodeLines = [
    { tag: 'div', closeTag: false, idName: 'ditc-home' },
    { tag: 'div', closeTag: false, className: 'ditc-row' },
    { tag: 'div', closeTag: false, className: 'ditc-col' },
    { tag: 'ng-template', closeTag: true, referenceName: 'contentTemplateRef' },
    { tag: '', closeTag: false, content: '!-- Content to be rendered --' },
  ];
}
