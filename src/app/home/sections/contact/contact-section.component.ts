import {Component} from '@angular/core';
import * as homePageConstants from '../../index';

@Component({
  selector: 'ditc-contact-section',
  template: `
    <div [id]="contactFragment" class="ditc-contact-section">
      <div class="ditc-container">
        <h3 class="ditc-title" i18n>Contact me</h3>

        <div style="display: flex; justify-content: space-between; flex-wrap: wrap">
          <div style="width: 33.5rem; height: 32.75rem; background-color: #C4C4C4; flex-basis: 50%; flex-grow: 1; padding: 0 1.875rem"></div>

          <div style="display: flex; flex-direction: column; text-align: left; flex-basis: 50%; flex-grow: 1; padding: 0 1.875rem">
            <span><ng-container i18n>Phone</ng-container>: <strong><a href="tel:+39 3396 795 351">+39 3396 795 351</a></strong></span>
            <span><ng-container i18n>Email</ng-container>: <strong><a href="mailto:consulting@danielitc.dev">consulting@danielitc.dev</a></strong></span>
            <span><ng-container i18n>Address</ng-container>: <strong>via Fratelli Cairoli, 4 Chiaravalle (AN), 60033</strong></span>

            <div style="display: flex; flex-direction: column; box-shadow: 10px 10px 20px rgba(0, 0, 0, .1); margin-top: 1.5rem; padding: 5rem 3rem 3.75rem 3rem; background-color: #fff">
              <ditc-contact></ditc-contact>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./contact-section.component.scss']
})
export class ContactSectionComponent {
  readonly contactFragment = homePageConstants.contactFragment;
}
