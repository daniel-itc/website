import {Component} from '@angular/core';
import * as homePageConstants from '../../index';

@Component({
  selector: 'ditc-services-section',
  template: `
    <div [id]="servicesFragment" class="ditc-services-section">
      <div class="ditc-container">
        <h3 class="ditc-title" i18n>Offered services</h3>

        <div class="ditc-services-section__services">
          <ditc-service style="flex-basis: 30%;" name="Technical and functional analysis"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l"
                        icon="icon-analysis"
                        i18n-name i18n-description></ditc-service>
          <ditc-service style="flex-basis: 30%;" name="Backend services development"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l"
                        icon="icon-backend" i18n-name i18n-description></ditc-service>
          <ditc-service style="flex-basis: 30%;" name="iOS & Android app development"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l"
                        icon="icon-mobile-app" i18n-name i18n-description></ditc-service>

          <ditc-service style="flex-basis: 30%;" name="Web development"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l"
                        icon="icon-web-development" i18n-name i18n-description></ditc-service>
          <ditc-service style="flex-basis: 30%;" name="Infrastructure development"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l"
                        icon="icon-infrastructure" i18n-name i18n-description></ditc-service>
          <ditc-service style="flex-basis: 30%;" name="Equipment installation"
                        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut l"
                        icon="icon-server" i18n-name i18n-description></ditc-service>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./services-section.component.scss']
})
export class ServicesSectionComponent {
  readonly servicesFragment = homePageConstants.servicesFragment;
}
