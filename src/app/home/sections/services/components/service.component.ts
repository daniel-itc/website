import {Component, Input} from '@angular/core';

@Component({
  selector: 'ditc-service',
  template: `
    <div class="ditc-service">
      <span class="ditc-service__title">{{name}}</span>
      <div class="ditc-service__icon">
        <span [ngClass]="icon"></span>
      </div>
      <span class="ditc-service__description">{{description}}</span>
    </div>
  `,
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent {
  @Input()
  name = '';

  @Input()
  icon: '';

  @Input()
  description = '';
}
