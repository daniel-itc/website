import {Component} from '@angular/core';

@Component({
  selector: 'ditc-portfolio-section',
  templateUrl: './portfolio-section.component.html',
  styleUrls: ['./portfolio-section.component.scss']
})
export class PortfolioSectionComponent { }
