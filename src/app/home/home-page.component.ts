import {Component} from '@angular/core';

@Component({
  template: `
    <ditc-top-bar></ditc-top-bar>

    <div class="ditc-home__introduction">
      <div class="ditc-home__introduction-overlay"></div>

      <div class="ditc-home__introduction-content">
        <span i18n>Welcome to</span>

        <div class="ditc-home__introduction-logo-container"></div>
        <picture class="ditc-home__introduction-logo">
          <source media="(min-width:465px)" srcset="assets/images/introduction-logo.png" type="image/png">
          <img height="113" src="assets/images/introduction-logo.png" alt="Daniel ITC Logo">
        </picture>
      </div>
    </div>

    <ditc-about-me-section></ditc-about-me-section>

    <ditc-target-section></ditc-target-section>

    <ditc-partners-section></ditc-partners-section>

    <ditc-services-section></ditc-services-section>

    <ditc-contact-section></ditc-contact-section>
  `,
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {}
