// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBn-nYyVAe_lTcvhGOr34oRUoSlHKYW5ic',
    authDomain: 'daniel-itc.firebaseapp.com',
    databaseURL: 'https://daniel-itc.firebaseio.com',
    projectId: 'daniel-itc',
    storageBucket: 'daniel-itc.appspot.com',
    messagingSenderId: '965884706260',
    appId: '1:965884706260:web:457db99d7cb7f51a356a41'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error';  // Included with Angular CLI.
